Synopsis:
	This is a simplistic web content management system that uses Node.js.

Configuration:
	The web content to be displayed on the webpage is located in the WebContent.xml file.  Each WebPage must have <Name> and <Content> tag.

	Here is an example:
		<WebPage>
			<Name>Home</Name>
			<Content>This is home.  There's no place like it.</Content>
		</WebPage>
	
	A misconfigured .xml file may prevent the program from executing correctly.

Motivation:
	I created this simplistic application in order to learn more about Node.js.  I am simply a beginner in the language, so any feedback on how to improve the program is appreciated.

Installation/Execution:
	You must have Node.js installed on your machine, to run this program.

	Once it is installed.  Download this project code, open a console in the project directory, and enter "node server.js" at the command prompt.  This will start the server.

	With the server running, open a web browser and navigate to http://localhost:8081/index.html.  The web content will appear there.
