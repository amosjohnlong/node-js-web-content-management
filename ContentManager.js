var fs = require("fs");
var xml2js = require("xml2js");

const WEB_CONTENT_FILE = 'WebContent.xml';

module.exports = {
	GetMenuOptions: function(callback) {
		fs.readFile(WEB_CONTENT_FILE, function (err, xmlData) {
			if (err) return console.error(err);
		   
			xml2js.parseString(xmlData, function (err, result) {
				if (err) return console.error(err);

				var menuOptionArray = [];
				var pageArray = result['WebPages'].WebPage;
				for (var index = 0; index < pageArray.length; ++index) {
					menuOptionArray.push(pageArray[index]['Name'][0]);
				}
				callback(menuOptionArray);
			})
		});
	},

	GetContent: function(webPageName, callback) {
		fs.readFile(WEB_CONTENT_FILE, function (err, xmlData) {
			if (err) return console.error(err);
		   
			xml2js.parseString(xmlData, function (err, result) {
				if (err) return console.error(err);

				var webPageContent;
				var pageArray = result['WebPages'].WebPage;
				for (var index = 0; index < pageArray.length; ++index) {
					if (webPageName == pageArray[index]['Name'][0]) {
						webPageContent = pageArray[index]['Content'][0];
						break;
					}
				}
				callback(webPageContent);
			})
		});
	},
};