var express = require('express');
var url = require('url');
var fs = require('fs');
var manager = require('./ContentManager');

const HTML_CONTENT_TYPE = 'text/html';
const CSS_CONTENT_TYPE = 'text/css';

var app = express();

var SendResponse = function(contentType, data, response) {
	response.writeHead(200, {'Content-Type': contentType});
	response.end(data.toString());
}

var SendErrorResponse = function(response) {
	response.writeHead(404, {'Content-Type': HTML_CONTENT_TYPE});
	response.end();
}

var SendEntireFileInResponse = function(contentType, pathName, response) {
	fs.readFile(pathName, function (err, data) {
		if (err) {
			console.log('Cannot find requested file: ' + pathName + '. Returning a 404 error response.');
			SendErrorResponse(response);
		} else {	
			SendResponse(contentType, data, response);	
		}
	});   	
}

var RemoveSpecialCharacters = function(path) {
	// Remove special characters from a path, except for . and /
	// NOTE: In the future may need to permit other characters like ? and &
	path = path.replace(new RegExp('[^a-zA-Z0-9./]', 'g'), '');
	path = path.replace(new RegExp('\\.{2}', 'g'), '');
	console.log('Stripped URL is', path);
	return path;
}

app.get('/Menu', function (request, response) {
	manager.GetMenuOptions(function(data) {
		menuOptionsAsJSON = JSON.stringify({ menuOptions: data });
		SendResponse(HTML_CONTENT_TYPE, menuOptionsAsJSON, response);
	});
})

app.get('/Page/:name', function (request, response) {
	var webPageName = request.params.name;
	
	manager.GetContent(webPageName, function(data) {
		contentAsJSON = JSON.stringify({ webPageContent: data});
		SendResponse(HTML_CONTENT_TYPE, contentAsJSON, response);
	})
})

app.get('/*.html', function (request, response) {
	// Retrieve file name from request object, ommitting the initial "/"
	var pathName = RemoveSpecialCharacters(url.parse(request.url).pathname.substr(1));

	SendEntireFileInResponse(HTML_CONTENT_TYPE, pathName, response);
})

app.get('/*.css', function (request, response) {
	// Retrieve file name from request object, ommitting the initial "/"
	var pathName = RemoveSpecialCharacters(url.parse(request.url).pathname.substr(1));

	SendEntireFileInResponse(CSS_CONTENT_TYPE, pathName, response);
})

var server = app.listen(8081, function () {
	console.log("Example app listening at http://localhost:%s", server.address().port);
})
